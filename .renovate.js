module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.com/api/v4/',
  assignees: ['rynr'],
  baseBranches: ['master'],
  labels: ['renovate'],
  extends: ['config:base'],
  repositories: ['rynr/kaas']
};
