# Kudos - As A Service

The live platform can be found at [`kaas.ws`](https://kaas.ws/).

## Local development

Install [`docker`](https://docs.docker.com/engine/install/) and
[`docker-compose`](https://docs.docker.com/compose/install/) and you're good to
go.

Checkout the [git repository](https://gitlab.com/rynr/kaas), change in the
projects directory and start with `docker-compose up`.  
Once all dependencies are downloaded and started, you can now access the local
version at [`http://localhost/`](http://localhost/).

Example session:

```
$ mkdir kaas && cd kaas
$ git clone git@gitlab.com:rynr/kaas.git .
Cloning into '.'...
remote: Enumerating objects: 43, done.
remote: Counting objects: 100% (43/43), done.
remote: Compressing objects: 100% (33/33), done.
remote: Total 924 (delta 21), reused 18 (delta 10), pack-reused 881
Receiving objects: 100% (924/924), 112.25 KiB | 631.00 KiB/s, done.
Resolving deltas: 100% (459/459), done.
$ docker-compose up
Starting kaas_kaas_1 ... done
Attaching to kaas_kaas_1
kaas_1  | [INFO] Scanning for projects...
kaas_1  | [INFO]
kaas_1  | [INFO] ------------------------------< ws:kaas >-------------------------------
kaas_1  | [INFO] Building kaas 0.1.1-SNAPSHOT
kaas_1  | [INFO] --------------------------------[ jar ]---------------------------------
kaas_1  | [INFO]
kaas_1  | [INFO] >>> spring-boot-maven-plugin:2.3.4.RELEASE:run (default-cli) > test-compile @ kaas >>>
kaas_1  | [INFO]
kaas_1  | [INFO] --- git-commit-id-plugin:3.0.1:revision (default) @ kaas ---
kaas_1  | [INFO]
kaas_1  | [INFO] --- git-commit-id-plugin:3.0.1:revision (get-the-git-infos) @ kaas ---
kaas_1  | [INFO]
kaas_1  | [INFO] --- maven-resources-plugin:3.1.0:resources (default-resources) @ kaas ---
kaas_1  | [INFO] Using 'UTF-8' encoding to copy filtered resources.
kaas_1  | [INFO] Copying 2 resources
kaas_1  | [INFO] Copying 9 resources
kaas_1  | [INFO]
kaas_1  | [INFO] --- maven-compiler-plugin:3.8.1:compile (default-compile) @ kaas ---
kaas_1  | [INFO] Nothing to compile - all classes are up to date
kaas_1  | [INFO]
kaas_1  | [INFO] --- maven-resources-plugin:3.1.0:testResources (default-testResources) @ kaas ---
kaas_1  | [INFO] Using 'UTF-8' encoding to copy filtered resources.
kaas_1  | [INFO] Copying 2 resources
kaas_1  | [INFO]
kaas_1  | [INFO] --- maven-compiler-plugin:3.8.1:testCompile (default-testCompile) @ kaas ---
kaas_1  | [INFO] Nothing to compile - all classes are up to date
kaas_1  | [INFO]
kaas_1  | [INFO] <<< spring-boot-maven-plugin:2.3.4.RELEASE:run (default-cli) < test-compile @ kaas <<<
kaas_1  | [INFO]
kaas_1  | [INFO]
kaas_1  | [INFO] --- spring-boot-maven-plugin:2.3.4.RELEASE:run (default-cli) @ kaas ---
kaas_1  | [INFO] Attaching agents: []
kaas_1  | 10:24:15.495 [restartedMain] INFO  ws.kaas.Application - Starting Application on 3f5a9a39790d with PID 40 (/app/target/classes started by root in /app)
kaas_1  | 10:24:15.497 [restartedMain] INFO  ws.kaas.Application - No active profile set, falling back to default profiles: default
kaas_1  | 10:24:15.536 [restartedMain] INFO  o.s.b.d.e.DevToolsPropertyDefaultsPostProcessor - Devtools property defaults active! Set 'spring.devtools.add-properties' to 'false' to disable
kaas_1  | 10:24:16.257 [restartedMain] INFO  o.s.b.w.e.tomcat.TomcatWebServer - Tomcat initialized with port(s): 8080 (http)
kaas_1  | 10:24:16.264 [restartedMain] INFO  o.a.coyote.http11.Http11NioProtocol - Initializing ProtocolHandler ["http-nio-8080"]
kaas_1  | 10:24:16.265 [restartedMain] INFO  o.a.catalina.core.StandardService - Starting service [Tomcat]
kaas_1  | 10:24:16.265 [restartedMain] INFO  o.a.catalina.core.StandardEngine - Starting Servlet engine: [Apache Tomcat/9.0.38]
kaas_1  | 10:24:16.311 [restartedMain] INFO  o.a.c.c.C.[Tomcat].[localhost].[/] - Initializing Spring embedded WebApplicationContext
kaas_1  | 10:24:16.311 [restartedMain] INFO  o.s.b.w.s.c.ServletWebServerApplicationContext - Root WebApplicationContext: initialization completed in 775 ms
kaas_1  | 10:24:16.562 [restartedMain] INFO  o.s.s.c.ThreadPoolTaskExecutor - Initializing ExecutorService 'applicationTaskExecutor'
kaas_1  | 10:24:16.703 [restartedMain] INFO  o.s.b.d.a.OptionalLiveReloadServer - LiveReload server is running on port 35729
kaas_1  | 10:24:16.707 [restartedMain] INFO  o.s.b.a.e.web.EndpointLinksResolver - Exposing 1 endpoint(s) beneath base path '/actuator'
kaas_1  | 10:24:16.724 [restartedMain] INFO  o.a.coyote.http11.Http11NioProtocol - Starting ProtocolHandler ["http-nio-8080"]
kaas_1  | 10:24:16.735 [restartedMain] INFO  o.s.b.w.e.tomcat.TomcatWebServer - Tomcat started on port(s): 8080 (http) with context path ''
kaas_1  | 10:24:16.751 [restartedMain] INFO  ws.kaas.Application - Started Application in 1.506 seconds (JVM running for 1.874)
```

You can stop it with `Ctrl-C`.

## Frameworks

[`kaas.ws`](https://kaas.ws/) uses as main libraries [spring boot](https://spring.io/projects/spring-boot), [mustache](https://mustache.github.io/) and [micrometer](https://micrometer.io/).
