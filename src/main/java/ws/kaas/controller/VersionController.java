package ws.kaas.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class VersionController {

  @GetMapping(value = "/version", produces = MediaType.TEXT_PLAIN_VALUE)
  public ResponseEntity<String> index() {
    final String version = System.getenv("VERSION");
    return ResponseEntity.ok(version != null && version.length() > 0 ? version : "unknown");
  }
}
