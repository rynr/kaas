package ws.kaas.controller;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import lombok.RequiredArgsConstructor;
import ws.kaas.configuration.KudoConfiguration;
import ws.kaas.configuration.KudoTemplate;
import ws.kaas.exception.KudoNotFoundException;

@Controller
@RequiredArgsConstructor
public class KudoController {
  @Autowired
  private final KudoConfiguration configuration;

  @GetMapping(value = "/{key:[\\w]+}", produces = MediaType.TEXT_HTML_VALUE)
  public ModelAndView clean(@PathVariable("key") final String key, final HttpServletRequest request)
      throws KudoNotFoundException {
    final KudoTemplate template =
        configuration.findByKey(key).orElseThrow(() -> new KudoNotFoundException(key));
    return new ModelAndView("kudo",
        Map.of("message", template.getClean(), "key", key, "path", request.getRequestURI()));
  }

  @GetMapping(value = "/{key:[\\w]+}/{name}", produces = MediaType.TEXT_HTML_VALUE)
  public ModelAndView name(@PathVariable("key") final String key,
      @PathVariable("name") final String name, final HttpServletRequest request)
      throws KudoNotFoundException {
    final KudoTemplate template =
        configuration.findByKey(key).orElseThrow(() -> new KudoNotFoundException(key));
    return new ModelAndView("kudo", Map.of("message", String.format(template.getTemplate(), name),
        "key", key, "path", request.getRequestURI()));
  }

  @GetMapping(value = "/{key:[\\w]+}/{name}/{from}", produces = MediaType.TEXT_HTML_VALUE)
  public ModelAndView nameFrom(@PathVariable("key") final String key,
      @PathVariable("name") final String name, @PathVariable("from") final String from,
      final HttpServletRequest request) throws KudoNotFoundException {
    final KudoTemplate template =
        configuration.findByKey(key).orElseThrow(() -> new KudoNotFoundException(key));
    return new ModelAndView("kudo", Map.of("message", String.format(template.getTemplate(), name),
        "from", from, "key", key, "path", request.getRequestURI()));
  }

}
