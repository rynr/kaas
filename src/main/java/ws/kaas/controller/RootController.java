package ws.kaas.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ws.kaas.configuration.KudoConfiguration;
import ws.kaas.configuration.KudoLink;
import ws.kaas.configuration.NameProvider;

@Controller("/")
public class RootController {

  private final NameProvider nameProvider;
  private final KudoConfiguration configuration;

  public RootController(final NameProvider nameProvider, final KudoConfiguration configuration) {
    this.nameProvider = nameProvider;
    this.configuration = configuration;
  }

  @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
  public ModelAndView index() {
    final String name = nameProvider.randomName();
    final String fromName = nameProvider.randomName();
    final List<KudoLink> links = new ArrayList<>();
    links.add(new KudoLink("<a href=\"/examples\">/examples</a>",
        "Some example links you can generate.", "examples"));
    links.add(new KudoLink("<a href=\"/version\">/version</a>", "Retrieve the version of kaas.ws.",
        "version"));
    configuration.getKudos().forEach(kudo -> links.addAll(kudo.getLinks(name, fromName)));
    links.sort((a, b) -> a.getSort().compareTo(b.getSort()));
    return new ModelAndView("root", Map.of("name", name, "fromName", fromName, "links", links));
  }
}
