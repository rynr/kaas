package ws.kaas.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ws.kaas.configuration.KudoConfiguration;
import ws.kaas.configuration.NameProvider;

@Controller
public class ExamplesController {

  private final Random random;
  private final NameProvider nameProvider;
  private final KudoConfiguration configuration;

  public ExamplesController(final NameProvider nameProvider,
      final KudoConfiguration configuration) {
    random = new Random();
    this.nameProvider = nameProvider;
    this.configuration = configuration;
  }

  @GetMapping(value = "/examples", produces = MediaType.TEXT_HTML_VALUE)
  public ModelAndView index() throws IOException {
    return new ModelAndView("examples", Map.of("examples", getExamples()));
  }

  private List<String> getExamples() {
    final ArrayList<String> keys = new ArrayList<>(configuration.getKeys());
    return IntStream.range(0, 20)
        .mapToObj(num -> keys.get(random.nextInt(keys.size())) + "/" + nameProvider.randomName()
            + (random.nextBoolean() ? "" : "/" + nameProvider.randomName()))
        .collect(Collectors.toList());
  }
}
