package ws.kaas.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class KudoNotFoundExceptionHandler extends ResponseEntityExceptionHandler {

  private HttpHeaders httpHeaders;

  @ExceptionHandler(value = {KudoNotFoundException.class})
  protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
    String bodyOfResponse = "file not found";
    return handleExceptionInternal(ex, bodyOfResponse, getHeaders(), HttpStatus.NOT_FOUND, request);
  }

  private HttpHeaders getHeaders() {
    if (httpHeaders == null) {
      httpHeaders = new HttpHeaders();
      httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE);
    }
    return httpHeaders;
  }

}
