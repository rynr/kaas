package ws.kaas.exception;

public class KudoNotFoundException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public KudoNotFoundException(final String key) {
    super(key + " not found");
  }
}
