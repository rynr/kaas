package ws.kaas.configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class NameProvider {

  private List<String> names;
  private Random random;

  public String randomName() {
    try {
      return getNames().get(getRandom().nextInt(getNames().size()));
    } catch (final IOException e) {
      return "Chuck Norris";
    }
  }

  private Random getRandom() {
    if (random == null) {
      random = new Random();
    }
    return random;
  }

  private List<String> getNames() throws IOException {
    if (names == null || names.size() < 1) {
      final ClassLoader classLoader = ClassLoader.getSystemClassLoader();
      try (InputStream is = classLoader.getResourceAsStream("names.txt")) {
        if (is == null) {
          return null;
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
          return reader.lines().filter(s -> !s.isEmpty()).collect(Collectors.toList());
        }
      }
    }
    return names;
  }

}
