package ws.kaas.configuration;

import java.io.Serializable;
import lombok.Data;

@Data
public class KudoLink implements Serializable {
  private static final long serialVersionUID = -6143014879556751334L;

  private final String link;
  private final String description;
  private final String sort;
}
