package ws.kaas.configuration;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class KudoTemplate {

  private String key;
  private String clean;
  private String template;

  public List<KudoLink> getLinks(final String name, final String from) {
    return List.of(new KudoLink("<a href=\"/" + key + "\">/" + key + "</a>", clean, key),
        new KudoLink("<a href=\"/" + key + "/" + name + "\">/" + key + "/<em>name</em></a>",
            String.format(template, "<em>name</em>"), key + "/name"),
        new KudoLink("<a href=\"/" + key + "/" + name + "/"+from+"\">/" + key + "/<em>name</em>/<em>from-name</em></a>",
            String.format(template, "<em>name</em>") + " (from: <em>from-name</em>)", key + "/name/from-name"));
  }

}
