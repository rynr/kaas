package ws.kaas.configuration;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Configuration
@ConfigurationProperties(prefix = "kaas")
@EqualsAndHashCode
@ToString
public class KudoConfiguration implements Serializable {
  private static final long serialVersionUID = 2871109098940164207L;

  private Map<String, KudoTemplate> kudos;

  public Collection<KudoTemplate> getKudos() {
    return kudos == null ? Set.of() : kudos.values();
  }

  public void setKudos(final Collection<KudoTemplate> kudos) {
    this.kudos =
        kudos.stream().collect(Collectors.toMap(KudoTemplate::getKey, Function.identity()));
  }

  public Optional<KudoTemplate> findByKey(final String key) {
    return kudos == null ? Optional.empty() : Optional.ofNullable(kudos.get(key));
  }

  public Set<String> getKeys() {
    return kudos == null ? Set.of() : kudos.keySet();
  }

}
