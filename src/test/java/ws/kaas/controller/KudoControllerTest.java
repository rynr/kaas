package ws.kaas.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import ws.kaas.configuration.KudoConfiguration;
import ws.kaas.configuration.KudoTemplate;
import ws.kaas.exception.KudoNotFoundException;

@ExtendWith(MockitoExtension.class)
class KudoControllerTest {

  @InjectMocks
  private KudoController sut; // System Under Test

  @Mock
  private KudoConfiguration configuration;

  @Test
  void shouldReturnCleanKudoModelAndView() throws KudoNotFoundException {
    when(configuration.findByKey("key"))
        .thenReturn(Optional.of(new KudoTemplate("key", "clean", "template for %s")));

    final ModelAndView result = sut.clean("key", new MockHttpServletRequest("GET", "/key"));

    final Map<String, Object> model = result.getModel();
    assertThat(result.getViewName(), equalTo("kudo"));
    assertThat(model.get("key"), equalTo("key"));
    assertThat(model.get("path"), equalTo("/key"));
    assertThat(model.get("message"), equalTo("clean"));
  }

  @Test
  void shouldReturnNameKudoModelAndView() throws KudoNotFoundException {
    when(configuration.findByKey("key"))
        .thenReturn(Optional.of(new KudoTemplate("key", "clean", "template for %s")));

    final ModelAndView result =
        sut.name("key", "John Doe", new MockHttpServletRequest("GET", "/key/John%20Doe"));

    final Map<String, Object> model = result.getModel();
    assertThat(result.getViewName(), equalTo("kudo"));

    assertThat(model.get("key"), equalTo("key"));
    assertThat(model.get("path"), equalTo("/key/John%20Doe"));
    assertThat(model.get("message"), equalTo("template for John Doe"));
  }

  @Test
  void shouldReturnNameFromKudoModelAndView() throws KudoNotFoundException {
    when(configuration.findByKey("key"))
        .thenReturn(Optional.of(new KudoTemplate("key", "clean", "template for %s")));

    final ModelAndView result = sut.nameFrom("key", "John Doe", "Jane Doe",
        new MockHttpServletRequest("GET", "/key/John%20Doe/Jane%20Doe"));

    final Map<String, Object> model = result.getModel();
    assertThat(result.getViewName(), equalTo("kudo"));

    assertThat(model.get("key"), equalTo("key"));
    assertThat(model.get("path"), equalTo("/key/John%20Doe/Jane%20Doe"));
    assertThat(model.get("message"), equalTo("template for John Doe"));
    assertThat(model.get("from"), equalTo("Jane Doe"));
  }

  @Test
  void shouldThrowExceptionOnCleanWithoutKudo() throws KudoNotFoundException {
    when(configuration.findByKey("key")).thenReturn(Optional.empty());

    final KudoNotFoundException exception = Assertions.assertThrows(KudoNotFoundException.class,
        () -> sut.clean("key", new MockHttpServletRequest("GET", "/key")));

    assertThat(exception.getMessage(), equalTo("key not found"));
  }

  @Test
  void shouldThrowExceptionOnNameWithoutKudo() throws KudoNotFoundException {
    when(configuration.findByKey("key")).thenReturn(Optional.empty());

    final KudoNotFoundException exception = Assertions.assertThrows(KudoNotFoundException.class,
        () -> sut.name("key", "John Doe", new MockHttpServletRequest("GET", "/key/John%20Doe")));

    assertThat(exception.getMessage(), equalTo("key not found"));
  }

  @Test
  void shouldThrowExceptionOnNameFromWithoutKudo() throws KudoNotFoundException {
    when(configuration.findByKey("key")).thenReturn(Optional.empty());

    final KudoNotFoundException exception =
        Assertions.assertThrows(KudoNotFoundException.class, () -> sut.nameFrom("key", "John Doe",
            "Jane Doe", new MockHttpServletRequest("GET", "/key/John%20Doe/Jane%20Doe")));

    assertThat(exception.getMessage(), equalTo("key not found"));
  }

}
