package ws.kaas.controller;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.servlet.ModelAndView;
import ws.kaas.configuration.KudoConfiguration;
import ws.kaas.configuration.NameProvider;

@ExtendWith(MockitoExtension.class)
class ExamplesControllerTest {

  @InjectMocks
  private ExamplesController sut; // System Under Test

  @Mock
  private NameProvider nameProvider;
  @Mock
  private KudoConfiguration configuration;

  @Test
  void shouldReturnMinorModelAndView() throws IOException {
    when(configuration.getKeys()).thenReturn(Set.of("key"));
    when(nameProvider.randomName()).thenReturn("John Doe");
    final ModelAndView result = sut.index();

    final Map<String, Object> model = result.getModel();
    assertThat(result.getViewName(), equalTo("examples"));
    @SuppressWarnings("unchecked")
    List<String> examples = (List<String>) model.get("examples");
    assertThat(examples.size(), equalTo(20));
    examples.forEach(example -> assertThat(example,
        anyOf(equalTo("key/John Doe"), equalTo("key/John Doe/John Doe"))));
  }

}
