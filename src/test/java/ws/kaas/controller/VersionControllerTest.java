package ws.kaas.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

class VersionControllerTest {

  @Test
  void shouldReturnUnknown() {
    // Only testing unknown, as the version is taken from the system, it's written from the
    // commandline.
    MatcherAssert.assertThat(new VersionController().index(),
        equalTo(ResponseEntity.ok("unknown")));
  }

}
