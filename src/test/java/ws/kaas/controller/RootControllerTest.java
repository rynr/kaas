package ws.kaas.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.servlet.ModelAndView;
import ws.kaas.configuration.KudoConfiguration;
import ws.kaas.configuration.KudoLink;
import ws.kaas.configuration.KudoTemplate;
import ws.kaas.configuration.NameProvider;

@ExtendWith(MockitoExtension.class)
class RootControllerTest {

  @InjectMocks
  private RootController sut; // System Under Test

  @Mock
  private NameProvider nameProvider;
  @Mock
  private KudoConfiguration kudoConfiguration;

  @Test
  void shouldReturnMinorModelAndView() {
    when(nameProvider.randomName()).thenReturn("John Doe");
    when(kudoConfiguration.getKudos()).thenReturn(Set.of());

    final ModelAndView result = sut.index();

    final Map<String, Object> model = result.getModel();
    assertThat(result.getViewName(), equalTo("root"));
    assertThat(model.get("name"), equalTo("John Doe"));
    assertThat(model.get("fromName"), equalTo("John Doe"));
    assertThat(model.get("links"),
        equalTo(List.of(new KudoLink("<a href=\"/examples\">/examples</a>",
            "Some example links you can generate.", "examples"), new KudoLink("<a href=\"/version\">/version</a>",
                "Retrieve the version of kaas.ws.", "version"))));
  }

  @SuppressWarnings("unchecked")
  @Test
  void shouldReturnOrderedLinksInModel() {
    when(nameProvider.randomName()).thenReturn("John Doe");
    when(kudoConfiguration.getKudos())
        .thenReturn(Set.of(new KudoTemplate("aaa", "pure aaa", "template aaa"),
            new KudoTemplate("zzz", "pure zzz", "template zzz")));

    final ModelAndView result = sut.index();

    final List<String> sortedFields = ((List<KudoLink>) result.getModel().get("links")).stream()
        .map(KudoLink::getSort).collect(Collectors.toList());
    assertThat(sortedFields, equalTo(List.of("aaa", "aaa/name", "aaa/name/from-name", "examples",
        "version", "zzz", "zzz/name", "zzz/name/from-name")));
  }

}
