package ws.kaas.exception;

import static org.hamcrest.CoreMatchers.equalTo;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

class KudoNotFoundExceptionTest {

  @Test
  void shouldContainTheMessageOfTheKeyNotFound() {
    MatcherAssert.assertThat(new KudoNotFoundException("key").getMessage(),
        equalTo("key not found"));
  }

}
