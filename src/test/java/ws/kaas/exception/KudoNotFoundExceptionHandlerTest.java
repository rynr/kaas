package ws.kaas.exception;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

@ExtendWith(MockitoExtension.class)
class KudoNotFoundExceptionHandlerTest {

  @InjectMocks
  KudoNotFoundExceptionHandler sut;
  @Mock
  WebRequest webRequest;

  @Test
  void should() {
    final ResponseEntity<Object> result =
        sut.handleConflict(new KudoNotFoundException("key"), webRequest);

    assertThat(result.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    assertThat(result.getBody(), equalTo("file not found"));
    assertThat(result.getHeaders().getContentType(), equalTo(MediaType.TEXT_PLAIN));
  }

}
