package ws.kaas.configuration;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.Test;

class KudoConfigurationTest {

  @Test
  void shouldHaveNoKeysByDefault() {
    final Set<String> result = new KudoConfiguration().getKeys();

    assertThat(result.size(), equalTo(0));
  }

  @Test
  void shouldHaveNoKudosByDefault() {
    final Collection<KudoTemplate> result = new KudoConfiguration().getKudos();

    assertThat(result.size(), equalTo(0));
  }

  @Test
  void shouldNotFindKudosByDefault() {
    final Optional<KudoTemplate> result = new KudoConfiguration().findByKey("key");

    assertThat(result.isEmpty(), equalTo(true));
  }

  @Test
  void shouldFindKeyOfDefinedKudo() {
    final KudoConfiguration sut = new KudoConfiguration();
    sut.setKudos(List.of(new KudoTemplate("key", "clean", "template")));

    final Set<String> result = sut.getKeys();

    assertThat(result, equalTo(Set.of("key")));
  }

  @Test
  void shouldHaveDefinedKudo() {
    final KudoConfiguration sut = new KudoConfiguration();
    final KudoTemplate expectedKudo = new KudoTemplate("key", "clean", "template");
    sut.setKudos(List.of(expectedKudo));

    final Collection<KudoTemplate> result = sut.getKudos();

    assertThat(result, hasItem(expectedKudo));
  }

  @Test
  void shouldFindDefinedKudo() {
    final KudoConfiguration sut = new KudoConfiguration();
    final KudoTemplate expectedKudo = new KudoTemplate("key", "clean", "template");
    sut.setKudos(List.of(expectedKudo));

    final Optional<KudoTemplate> result = sut.findByKey("key");

    assertThat(result.isEmpty(), equalTo(false));
    assertThat(result.get(), equalTo(expectedKudo));
  }

  @Test
  void shouldNotFindNotDefinedKudo() {
    final KudoConfiguration sut = new KudoConfiguration();
    final KudoTemplate expectedKudo = new KudoTemplate("key", "clean", "template");
    sut.setKudos(List.of(expectedKudo));

    final Optional<KudoTemplate> result = sut.findByKey("other");

    assertThat(result.isEmpty(), equalTo(true));
  }

}
