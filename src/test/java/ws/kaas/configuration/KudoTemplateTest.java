package ws.kaas.configuration;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import java.util.List;
import org.junit.jupiter.api.Test;

class KudoTemplateTest {

  @Test
  void shouldCreate() {
    final List<KudoLink> result =
        new KudoTemplate("key", "clean", "template").getLinks("Struppi", "Tim");

    assertThat(result.size(), equalTo(3));
    assertThat(result.get(0), equalTo(new KudoLink("<a href=\"/key\">/key</a>", "clean", "key")));
    assertThat(result.get(1), equalTo(
        new KudoLink("<a href=\"/key/Struppi\">/key/<em>name</em></a>", "template", "key/name")));
    assertThat(result.get(2), equalTo(
        new KudoLink("<a href=\"/key/Struppi/Tim\">/key/<em>name</em>/<em>from-name</em></a>",
            "template (from: <em>from-name</em>)", "key/name/from-name")));
  }

}
