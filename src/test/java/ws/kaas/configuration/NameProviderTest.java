package ws.kaas.configuration;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

class NameProviderTest {

  @Test
  @RepeatedTest(10)
  void shouldRetrieveNamesFromNamesTxtResource() {
    final String result = new NameProvider().randomName();

    // check the file `src/test/resources/names.txt`.
    // The file has an empty line at the end to verify it's not used as empty name.
    MatcherAssert.assertThat(result, anyOf(equalTo("Tim"), equalTo("Struppi")));
  }

}
